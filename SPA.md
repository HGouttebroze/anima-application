# SPA (Single Page Application)

## English
I advise you to start with tutorials to design your API with Symfony and to test it with Postman. Ideally, with PHPUnit tests, it's even better.

Then, you'll be able to create an application that will connect to this API, with Angular, React or vue (or even without

import the logic 
The ability to structure an application that uses Ajax to retrieve content from a remote location is one of the attractions of a single page application (SPA). Not only does it improve the performance of an application, but it also makes it easier to navigate between pages.

In this tutorial, you have learned how to build a single-page application, using Symfony as back-end and front-end logic powered by React. This allowed you to discover how easy it is to combine React and Symfony.


The advantages of an SPA 

but why change the code of a secure and functional application developed in PHP on Symfony?
we could perfectly put our web application in production and deploy it. 
Motivations to take back the code and create a SPA (Single Page Application), A SPA facilitates the navigation between the different pages ... The performance is improved, from the point of view of the developer, curious and fascinated by new technologies, which he feels the inevitable need to learn, discover, again and again ... but, as who is intended this application to therme? it is to users, and this is precisely where an SPA presents its interest, an SPA improve the user experience! 

Translated with www.DeepL.com/Translator (free version)

## French 
Je te conseille de commencer par des tutos pour concevoir ton API avec Symfony et de la tester avec Postman. Idéalement, avec des tests unitaires PHPUnit, c'est encore mieux.

Ensuite, tu pourras créer une application qui se connectera à cette API, et ce aussi bien avec Angular que React ou vue (ou même sans framework

deporter la logique 
La capacité à structurer une application qui fait appel à Ajax pour extraire du contenu à partir d'un emplacement distant est l'un des attraits d'une application monopage (SPA). Non seulement les performances d'une application s'en trouvent améliorées, mais cela facilite aussi la navigation entre les différentes pages.

Dans ce tutoriel, vous avez appris à construire une application monopage, en utilisant Symfony comme logique back-end et front-end alimentée par React. Cela vous a permis de découvrir à quel point il est facile de combiner React et Symfony.


Les avantages d'une SPA 

mais pourquoi changer le code d'une application sécurisée et fonctionnelle développé en PHP sur Symfony?
nous pourrions parfaitement mettre notre application web en production et la déployer. 
Les motivations pour reprendre le code et créer une SPA (Single Page Application),  Une SPA facilite la navigation entre les différentes pages ... Les performances sont améliorées, de point de vue du développeur, curieux et fasciné par les nouvelles technologies, dont il ressent le besoin inévitable d'appendre, découvrir, encore et encore ... mais, as qui est destiné cette application à therme? c'est aux utilisateurs, et c'est justement là ou une SPA présente son intérêt, une SPA améliorer l'expérience utilisateur ! 