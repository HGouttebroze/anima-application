# TODO

`php bin/console doctrine:fixtures:load --append`

## IN FORM CONTACT

- add user entity informations in my contact object
  (like firstname & lastname, phone, address, city and department residence...)

## CONTACT FORM: seul les chien non adopté peuvent faire l'objet d une demande:

ajouter 1 contrainte genre (?):

⚠️ un chien de l'annonce peut être adopté, mais pas tous.
Seuls les chiens non adoptés peuvent faire l'objet d'une demande d'

     * @Assert\Choice({"fiction", "non-fiction"})

## create a issue: Cacher les annonce où les dogs sont adopté

donc ça donnerais 1 truck ds le genre:
`(isTaken = true) ? hide : show;`

Changements dans le fonctionnel

Créer une branche

Si un chien est adopté :

s'il n'est pas seul dans une annonce, l'afficher sur l'annonce

s'il est seul dans une annonce, l'annonce est pourvue et peut donc être fermée

Une annonce pourvue n'apparait plus dans la liste

/!\ Pour ne plus afficher une annonce, vous pouvez ajouter une propriété (hidden, un booléen, par exemple) dans votre entité d'annonce, et créer une méthode dans votre repository pour ne récupérer que les annonces "visibles" (dans mon exemple, avec la propriété hiddenà false).

Créer un/des commits dans votre branche

Créer une Merge/Pull Request

Faites la relire par une ou deux personnes

## create a issue: mettre un lien sur les annonce vers contact

Contacter quelqu'un pour un chien
Notre formulaire de contact peut être lié à une annonce nous allons maintenant lier les deux

Créer une branche

Dans une page d'annonce, ajouter un lien vers la page /contact, avec un paramètre
supplémentaire : l'identifiant de l'annonce (utiliser le second paramètre de
path() pour transmettre ce paramètre)

Dans l'action de la page de contact, récupérer ce paramètre et, s'il n'est pas null, l'ajouter à l'objet contact.

Le champ indiquant l'annonce doit alors être rempli avec la bonne annonce (utiliser une champ de type EntityType pour avoir une liste déroulante)

# BUGS

- OK BUG REPARE: btn page contact n 'nvoie pas le form !!!!!!'

# FORMS ACCESS and define roles

###

Le site sera partagé en plusieurs espaces :

Une partie "adoptant", pour permettre de consulter et de répondre à une annonce
Une partie "mon compte annonceur" qui permettra aux éleveurs et aux SPA de poster et gérer leurs annonces
Une partie administration, utilisable uniquement par des administrateurs (une ou deux personnes)
#Partie "adoptant"
Ici, nous appelons "adoptant" tout visiteur du site souhaitant adopter un ou plusieurs chiens présentés sur le site. Ils doivent pouvoir :

consulter une annonce,
créer un compte (obligatoire pour répondre à une annonce)
envoyer une demande à une annonce ("je veux adopter ce(s) chien(s)")
être notifié (sur le site) et lire les réponses des éleveurs/SPA et échanger avec ces derniers (fil de discussion)
#Page d'accueil
La page d'accueil permettra de voir rapidement l'actualité du site
(dernières annonces) et de présenter les éleveurs et
SPA présentes sur le site (liens vers fiches de présentation).

Ces éléments seront répartis en 2 blocs :

une liste contenant les 5 dernières annonces (encore à pourvoir),
chaque annonce devant présenter :
les 3 premières photos de l'annonce
le titre de l'annonce
le nombre de chiens concernés
les races des chiens concernés
la date de mise à jour de l'annonce
Une liste exhaustive des éleveurs et des SPA présentes sur le site. Elles devront être présentés en fonction des dernières annonces mises à jour (plus l'association/SPA a mis à jour une annonce récemment, plus elle apparait en tête). Chaque élémént doit présenter :
Le nom de l'association/SPA
Le nombre d'annonces actuellement à pourvoir de cette association/SPA
Le nombre d'annonces pourvues de cette association/SPA

### ROLES ATRIBUTION

site espaces :

- "ROLE_USER" = Une partie "adoptant", pour permettre de consulter
  et de répondre à une annonce .
- "ROLE_ADMIN" = "mon compte annonceur" qui permettra aux
  éleveurs et aux SPA de poster
  et gérer leurs annonces.
- "ROLE_SUPER_ADMIN" = Une partie administration, utilisable
  uniquement par des administrateurs
  (une ou deux personnes).

# page annonce

Une page d'annonce doit comporter les éléments suivants :

## create Adoption entity

- Le titre de l'annonce
- informations sur les différents chiens adoptables.

### create Dog Entity

un chien doit avoir les attributs suivants:

- nom

- CREATE BREED Entity:
  sa ou ses races (au moins une,
  et prévoir "inconnue" pour les chiens croisés)
- ses antécédents (texte descriptif de son passé)

- s'il est LOF ou non (chien de race homologué)
- description (texte descriptif du chien et de son comportement)

- s'il accepte les autres animaux

- une à 5 images

- Un lien permettant de répondre à l'annonce (voir partie "Répondre
  à l'annonce")

- isTaken: ⚠️ un chien de l'annonce peut être adopté,
  mais pas tous. Seuls les chiens non adoptés peuvent faire
  l'objet d'une demande d'adoption

annonce:
name,
info dogs,
dog entity: name,
breed(avec inconnu? = voir mettre à NULL?),
past/history/life(text),
description(text),
LOF(bool),
sociability(bool),
1 à 5 img(faire 1 boucle for ds les fixtures par exemple...),
isTaken(bloqué si taken !!!),

# TODO

creer fixtures: 5 annonces pr home âge
presenter les association spa

Pouvoir poster une annonce en tant qu'annonceur (admin) et
y répondre en tant qu'adoptant (user)

AssociationEntity:

- un association possède 1 ou plusieurs annonce (announcement)
- une announcement est posté par 1 & 1 seul association
- une association a 1 ou plusieurs chien à placés
- les chiens sont placé par 1 ou des associations
  Une liste exhaustive des éleveurs et des SPA présentes sur le site.
  Elles devront être présentés en fonction des dernières annonces
  mises à jour (plus l'association/SPA a mis à jour une annonce
  récemment, plus elle apparait en tête). Chaque élémént doit présenter :
  Le nom de l'association/SPA
  Le nombre d'annonces actuellement à pourvoir de cette association/SPA
  Le nombre d'annonces pourvues de cette association/SPA

faire page inscription
announcementProvided
vacancyAnnouncement
ContactUserEntity:
(email, téléphone, ville et département de résidence, nom, prénom, etc.)
CompteUserEntity => accees au crud de ses infos persos:
Adoptant:modifier ses informations personnelles (
qui permettront de remplir plus rapidement le formulaire d'adoption)

# liens

securité, connexion:
https://formation-hb.drakolab.fr/symfony/20-user.html#dans-un-service

validation:
https://formation-hb.drakolab.fr/symfony/10-formulaires.html#validation
https://symfony.com/doc/current/validation.html#constraints

# MSG REMY DISCORD INDICATION DEMANDE CLIENT

Dreeckan — 01/08/2021
Bonsoir Hugues :slight_smile:
Comment vas-tu ?

Je ne t'ai pas oublié pour le sujet de rattrapage, je te l'envoie ce soir : https://formation-hb.drakolab.fr/symfony/98-projet.html

L'idée est de construire un site en Symfony, répondant à une demande
client (il faut donc l'interpréter et la réaliser). Idéalement,
je te conseillerais de mettre le projet sur Github/Gitlab
avant de l'envoyer sur Moodle (dans l'examen Symfony).

Tu as donc jusqu'au 1 septembre 12h pour rendre le projet ;).

Ce que je noterai :

- les fonctionnalités faites (tout n'a pas à être complètement
  terminé, mais j'attends certains éléments, que je vais noter après)
- le respect de la demande du client (si le cahier des charges
  n'est pas respecté sur ce qui est fait, je ferai le client
  mécontent :wink: )

Elements attendus :

- L'espace d'administration sera fait avec Easy Admin Bundle
- Pour remplir la base de données, il faudra utiliser des Fixtures
  (avec Doctrine Fixtures Bundle)
- La sécurisation (connexion en tant qu'admin, adoptant ou annonceur)
  doit être fonctionnelle pour les 3 types d'utilisateur
- Pouvoir poster une annonce en tant qu'annonceur et
  y répondre en tant qu'adoptant

Informations utiles :

- Le site n'a pas à être joli, juste fonctionnel (s'il n'est pas
  mis en forme, je ne m'en offusquerai pas :wink: )
- Tu peux bien sûr le rendre en avance si tu préfères
- Tu peux également me poser toutes les questions que tu souhaites,
  j'y répondrai dès que possible !

Voilà, belle soirée à toi et à très vite !

# HERE

- home page, j'ai que 2 annonces sur mes 3, respecter cahier des charges
  pr nbres annonces et nbres images

- https://127.0.0.1:8000/annonces/9#
  le lien "ns contacter" pour causer de l annonce ne marche pas

- voir pr pagination et faire 1 findbidultruck...

- ajouter et mettre 1 form pour que les users
  puissent renseigner nom , adresse, telephone...
  -> soit créé 1 entité adoptant?
  -> soit ajouter de param non obligatoire (qui peuvent donc
  être NULL !!!) ?
  -> pt etre avec les TRAILS ???
        BREF, A VOIr, MS TRES IMPORTANT !!!

- twig, faire des truc genre racourci adoption-truck-form...
- j ai 1 migration, voir ce ki a dedans et l annuler ? elle passe pas
  a cause d'une jointure avec dog_id (histoire de trail, de ....????S)

- faire fiches de présentation:
  présenter les éleveurs et
  SPA présentes sur le site (liens vers fiches de présentation

- random pr denerer de 1 a 5 img sur l annnce
