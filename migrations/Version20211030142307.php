<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211030142307 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE media (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(128) NOT NULL, description TEXT DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('DROP TABLE image');
        $this->addSql('ALTER TABLE adoption ADD user_id INT DEFAULT NULL, ADD created_at DATETIME NOT NULL, ADD updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE adoption ADD CONSTRAINT FK_EDDEB6A9A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_EDDEB6A9A76ED395 ON adoption (user_id)');
        $this->addSql('ALTER TABLE contact ADD zipcode VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE dog ADD user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE dog ADD CONSTRAINT FK_812C397DA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_812C397DA76ED395 ON dog (user_id)');
        $this->addSql('ALTER TABLE picture ADD dog_id INT DEFAULT NULL, ADD adoption_id INT DEFAULT NULL, ADD media_id INT DEFAULT NULL, CHANGE cat_id cat_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE picture ADD CONSTRAINT FK_16DB4F89634DFEB FOREIGN KEY (dog_id) REFERENCES dog (id)');
        $this->addSql('ALTER TABLE picture ADD CONSTRAINT FK_16DB4F89631C55DF FOREIGN KEY (adoption_id) REFERENCES adoption (id)');
        $this->addSql('ALTER TABLE picture ADD CONSTRAINT FK_16DB4F89EA9FDD75 FOREIGN KEY (media_id) REFERENCES media (id)');
        $this->addSql('CREATE INDEX IDX_16DB4F89634DFEB ON picture (dog_id)');
        $this->addSql('CREATE INDEX IDX_16DB4F89631C55DF ON picture (adoption_id)');
        $this->addSql('CREATE INDEX IDX_16DB4F89EA9FDD75 ON picture (media_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE picture DROP FOREIGN KEY FK_16DB4F89EA9FDD75');
        $this->addSql('CREATE TABLE image (id INT AUTO_INCREMENT NOT NULL, dog_id INT DEFAULT NULL, path VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_C53D045F634DFEB (dog_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE image ADD CONSTRAINT FK_C53D045F634DFEB FOREIGN KEY (dog_id) REFERENCES dog (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('DROP TABLE meuse Doctrine\Common\Collections\Collection;
dia');
        $this->addSql('ALTER TABLE adoption DROP FOREIGN KEY FK_EDDEB6A9A76ED395');
        $this->addSql('DROP INDEX IDX_EDDEB6A9A76ED395 ON adoption');
        $this->addSql('ALTER TABLE adoption DROP user_id, DROP created_at, DROP updated_at');
        $this->addSql('ALTER TABLE contact DROP zipcode');
        $this->addSql('ALTER TABLE dog DROP FOREIGN KEY FK_812C397DA76ED395');
        $this->addSql('DROP INDEX IDX_812C397DA76ED395 ON dog');
        $this->addSql('ALTER TABLE dog DROP user_id');
        $this->addSql('ALTER TABLE picture DROP FOREIGN KEY FK_16DB4F89634DFEB');
        $this->addSql('ALTER TABLE picture DROP FOREIGN KEY FK_16DB4F89631C55DF');
        $this->addSql('DROP INDEX IDX_16DB4F89634DFEB ON picture');
        $this->addSql('DROP INDEX IDX_16DB4F89631C55DF ON picture');
        $this->addSql('DROP INDEX IDX_16DB4F89EA9FDD75 ON picture');
        $this->addSql('ALTER TABLE picture DROP dog_id, DROP adoption_id, DROP media_id, CHANGE cat_id cat_id INT NOT NULL');
    }
}
