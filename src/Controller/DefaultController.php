<?php

namespace App\Controller;


use App\Entity\Adoptant;
use App\Entity\Adoption;
use App\Entity\Contact;

use App\Entity\ContactPreAdoption;
use App\Form\ContactFormType;
use App\Form\ContactType;
use App\Repository\AdoptionRepository;
use App\Repository\AssociationRepository;

use App\Notification\ContactNotification;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\FormTypeInterface;


class DefaultController extends AbstractController
{

    private $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }
    /**
     * @Route("/", name="default_index")
     * @param AdoptionRepository $adoptionRepository
     * @return Response
     */
    public function index(AdoptionRepository $adoptionRepository, AssociationRepository $associationRepository): Response
    {

       $adoptions = $adoptionRepository->findNotTaken(5, 'updatedAt');
       $associations = $associationRepository->findAll();

        return $this->render('default/index.html.twig', [
            'adoptions' => $adoptions,
            'associations' => $associations
        ]);
    }

    /**
     * @Route("/contact", name="default_contact")
     * @Route("/contact/{id}", name="default_contact_for_adoption")
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    public function contact(Request $request, EntityManagerInterface $entityManager, Adoption $adoption = null): Response
    {

        $this->denyAccessUnlessGranted('ROLE_USER'); // like if we put annotation @IsGranted but we can define roles redirection


        $contact = new Contact();

        $contact->setAdoption($adoption);

        $form = $this->createForm(ContactFormType::class, $contact);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            /*$notification->notify($contact);*/
            /*$mailer->sendEmail();*/

            //$email = (new Email())
            //    ->from('docteurfutur@gmail.com')
                //->from($contact->get('email'))
            //    ->to('docteurfutur@gmail.com')
                //->cc('cc@example.com')
                //->bcc('bcc@example.com')
           //     ->replyTo($contact->getEmail(''))
                //->priority(Email::PRIORITY_HIGH)
                //->subject('Time for Symfony Mailer!')
            //    ->text('Sending emails is fun again!')
             //   ->getHtmlBody($this->render('emails/contact.html.twig', [
            //        'contact' => $contact
            //    ]), 'text/html');
            //$this->mailer->send();
                //->html('<p>See Twig integration for better HTML integration!</p>');
            /*
                //->from('noreply@agence.fr')
                //->to('contact@anima.fr')
                //->replyTo($contact->getEmail());*/
                /**/

            


            //$mailer->send($email);

            //$context = [
              //  'adoption' => $adoption,
                //'mail' => $contact->get('email')->getData(),
                //'message' => $contact->get('message')->getData()
            //];

            //$mail->send($contact->get('email')->getData(), $annonce->getUsers()->getEmail(), 'Contact au sujet de votre annonce "' . $annonce->getTitle() . '"', 'contact_annonce', $context);

            // On confirme et on redirige
            //$this->addFlash('message', 'Votre e-mail a bien été envoyé');
            //return $this->redirectToRoute('annonces_details', ['slug' => $annonce->getSlug()]);
        //}


            $entityManager->persist($contact);
            $entityManager->flush();

            $this->addFlash('success', 'Votre demande de pré-adoption a bien été envoyé, merci !');

            return $this->redirectToRoute('default_contact');

           /* $this->addFlash('success', 'Votre message a bien été envoyé');

            return $this->redirectToRoute('default_index');*/
        }

        return $this->render('default/contact.html.twig', [
            'form' => $form->createView(),
        ]);


        /*        $contact = new Contact();
                $contact->setAdoption($adoption);

                $form = $this->createForm(ContactType::class, $contact);

                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {
                    $entityManager->persist($contact);
                    $entityManager->flush();

                    $this->addFlash('success', 'msg envoyé');

                    return $this->redirectToRoute('default_contact');
                }

                return $this->render('default/contact.html.twig', [
                    'form' => $form->createView(),
                ]);*/
    }

    /**
     * @Route("/conditions", name="default_conditions")
     */
    public function conditions(): Response
    {
        return $this->render('default/conditions.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }

    /**
     * @Route("/preadoption", name="default_preadoption")
     */
    public function preadoption(Request $request, EntityManagerInterface $entityManager, Adoption $adoption = null): Response
    {
        $preadoption = new ContactPreAdoption();
        $preadoption->setAdoption($adoption);

        $form = $this->createForm(ContactPreAdoption::class, $preadoption);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($preadoption);
            $entityManager->flush();

            $this->addFlash('success', 'Votre demande d\'adoption a bien été envoyée, Nous vous contacterons au plus vite');

            return $this->redirectToRoute('default_preadoption');
        }

        return $this->render('default/preadoption.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}

