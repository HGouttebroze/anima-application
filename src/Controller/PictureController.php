<?php

namespace App\Controller;

use App\Entity\Picture;
use App\Form\PictureType;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/image")
 */
class PictureController extends AbstractController
{
    /**
     * @Route("/nouvelle", name="picture_new")
     */
    public function new(EntityManagerInterface $em): Response
    {
        $picture = new Picture();

        $form = $this->createForm(PictureType::class, $picture);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($picture);
            $em->flush();

            return $this->redirectToRoute('picture_new');
        }

        return $this->render('picture/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
