<?php

namespace App\DataFixtures;

use App\Entity\Adoption;
use App\Entity\Association;
use App\Repository\AdoptionRepository;
use App\Repository\AssociationRepository;
use App\Repository\DogRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AssociationFixtures extends Fixture
{

    /**
     * @var DogRepository
     */
    private $dogRepository;
    /**
     * @var AdoptionRepository
     */
    private $adoptionRepository;

/*    public function __construct(DogRepository $dogRepository, AdoptionRepository $adoptionRepository)
    {
        $this->dogRepository = $dogRepository;
        $this->adoptionRepository = $adoptionRepository;
    }*/
    public function load(ObjectManager $manager)
    {
        /*
        $dogs = $this->dogRepository->findAll();
        $adoptions = $this->adoptionRepository->findAll();
        */
        // TODO: see if need to create a new methode

        $asso = new Association();
        $asso->setName('SPA');
        $asso->setEmail('spa@asso.fr');

        $manager->persist($asso);

        $asso1 = new Association();
        $asso1->setName('Vétérinaires-Loire');
        $asso1->setEmail('veto@loire.fr');
;
        $manager->persist($asso1);
        $manager->flush();

        /*$associationNames = [
            'Véterinaire Véto Véto',
            'SPA Saint-Etienne',
            'SPA Pilat',
            'SPA Annonay',
            'Vétérinaire Pélussin',
            'SPA Lyon',
        ];

        foreach ($associationNames as $associationName) {
            $asso2 = new Association();
            $asso2->setName($associationName);
            $manager->persist($asso2);
        }*/
    }

/*    public function getDependencies()
    {
        return [
            AdoptionFixtures::class,
        ];
    }*/

}
