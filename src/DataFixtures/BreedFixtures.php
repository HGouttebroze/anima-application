<?php


namespace App\DataFixtures;


use App\Entity\Breed;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class BreedFixtures extends Fixture
{
    /**
     * {@inheritDoc}
     */

    public function load(ObjectManager $manager)
    {
        // breed's objects creation (dummy datas), a cane corso, a boxer a labrador
        // READ THE FUCKIN DOC & => don t forget to take care on order datas fixtures creation

        $caneCorso = new Breed();
        $caneCorso->setName('Cane Corso');
        $caneCorso->setDescription('Le Cane Corso est un chien d’Italie. Chien de garde par excellence, il est surtout un adorable compagnon de vie pour les familles avec ou sans enfant. Il est doux, attentionné, fidèle et loyal. C’est un chien sportif qui a besoin d’une activité quotidienne. Il possède une santé robuste.');

        $manager->persist($caneCorso);

        $boxer = new Breed();
        $boxer->setName('Boxer');
        $boxer->setDescription('Le Boxer est un chien débordant d\'énergie, vif, gentil et affectueux.');

        $manager->persist($boxer);

        $labrador = new Breed();
        $labrador->setName('Labrador');
        $labrador->setDescription('Le Labrador est pateux, gentil, très calme si il se dépence comme il se doit.');

        $manager->persist($labrador);

        $manager->flush();
    }
}
