<?php


namespace App\DataFixtures;


use App\Entity\Cat;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CatFixtures extends Fixture
{

    public function load(ObjectManager $manager)
    {
        // Create dummy datas to create a data base unit test (SQLite DB)
        // (I create 18 cats, so I must find this number when testing)
        for($i = 1; $i < 19; $i++){
            $cats = new Cat();
            $cats->setName('Chats n°' .$i);
            $cats->setDescription('Miaou, je suis le chat n°' . $i . ', je ronronne beaucoup quand on me carresse et je 
            fait "MiaouMiaou". Je n\ai qu\'un numéro, le ' .$i. ', donnez moi un nom sympa... Adoptez moi! Miaou!');
            $cats->setPictureFiles('followYourDreams.jpg');
            $manager->persist($cats);
        }
         $manager->flush();
    }
}
