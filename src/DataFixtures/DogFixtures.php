<?php


namespace App\DataFixtures;


use App\Entity\Dog;
use App\Repository\AssociationRepository;
use App\Repository\ImageRepository;
use DateTime;
use App\Repository\BreedRepository;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class DogFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * @var BreedRepository
     */
    private $breedRepository;
    /**
     * @var AssociationRepository
     */
    private $associationRepository;
    /**
     * @var ImageRepository
     */
    private $imageRepository;

    public function __construct(BreedRepository $breedRepository, AssociationRepository $associationRepository)
    {
        $this->breedRepository = $breedRepository;
        $this->associationRepository = $associationRepository;

    }
    /**
     * {@inheritDoc}
     */

    public function load(ObjectManager $manager)
    {
        $boxer = $this->breedRepository->findOneBy(['name' => 'Boxer']);
        $labrador = $this->breedRepository->findOneBy(['name' => 'Labrador']);
        $caneCorso = $this->breedRepository->findOneBy(['name' => 'Cane Corso']);
        $associations = $this->associationRepository->findAll();

        $date = new DateTime('2012-08-12');

        $poupouille = new Dog();
        $poupouille->setName('Poupouille');
        $poupouille->setDescription('Trop jolie ce poilu');
        $poupouille->addBreed($boxer);
        $poupouille->addBreed($caneCorso);
        $poupouille->setHistory('des galères et des galères, rescapé...');
        $poupouille->setLOF(0);
        $poupouille->setSociabilityToAnimals(1);
        $poupouille->setBirthDate($date);

        $manager->persist($poupouille);

        $date = new DateTime('2005-01-02');

        $massacre = new Dog();
        $massacre->setName('Massacre');
        $massacre->setDescription('Trop gentil, sociable, besoin de compagnie et de longues ballades dans les bois, bref jolie programme pour un poilu adorable');
        $massacre->addBreed($labrador);
        $massacre->addBreed($caneCorso);
        $massacre->setHistory('des galères et des galères, la misère, rescapé...');
        $massacre->setSociabilityToAnimals('1');
        $massacre->setLOF('0');
        $massacre->setBirthDate($date);
        $massacre->setAssociation($associations[0]);

        $manager->persist($massacre);

        for ($i = 0; $i < 15; $i++) {
            $porteDeRescapeToutPouille = new Dog();
            $porteDeRescapeToutPouille->setName('Doggie numéro ' . $i . ' de la portée!');
            $porteDeRescapeToutPouille->setBirthDate(new \DateTime());
            $porteDeRescapeToutPouille->setDescription('Ben... on sait pas grand choses... mais c\'est le Doggie numéro ' . $i . ' de la portée!');
            $porteDeRescapeToutPouille->setSociabilityToAnimals(1);
            $porteDeRescapeToutPouille->setLOF(0);
            $porteDeRescapeToutPouille->setHistory('porté de rescapé');
            $porteDeRescapeToutPouille->addBreed($caneCorso);
            $porteDeRescapeToutPouille->addBreed($boxer);

            $randomNumber = mt_rand(0, count($associations) - 1);
            $porteDeRescapeToutPouille->setAssociation($associations[$randomNumber]);


            $manager->persist($porteDeRescapeToutPouille);
        }
        $manager->flush();


    }
    public function getDependencies()
    {
        return [
            BreedFixtures::class,
            ImageFixtures::class
        ];
    }
}
