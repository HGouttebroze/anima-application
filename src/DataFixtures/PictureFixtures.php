<?php


namespace App\DataFixtures;



use App\Entity\Picture;
use App\DataFixtures\CatFixtures;
use App\DataFixtures\DogFixtures;
use App\Repository\CatRepository;
use App\Repository\DogRepository;
use App\Repository\AdoptionRepository;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class PictureFixtures extends Fixture implements DependentFixtureInterface
{


    public function __construct(AdoptionRepository $adoptionRepository, DogRepository $dogRepository, CatRepository $catRepository)
    {
        $this->adoptionRepository = $adoptionRepository;
        $this->dogRepository = $dogRepository;
        $this->catRepository  = $catRepository;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {

        // create 16 pictures 
        for ($i = 1; $i < 16; $i++) {
            $picture1 = new Picture();
            $picture1->setImageFile('https://images.unsplash.com/photo-1508814437933-f0c7d18a9217?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MzR8fGRvZ3N8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60');
            $picture1->setMedia();
            $picture1->setUpdatedAt();
            $picture1->setFilename();
            $picture1->setDog();
            $picture1->setCreatedAt();
            $picture1->setCreatedAt();

            $randomNumber = mt_rand(0, count($pictures) - 1);
            $pictures->($pictures[$randomNumber]);

            $manager->persist($picture1);

         
        $manager->flush();

        /*    $dogs = $this->dogRepository->findAll();
        $adoptions = $this->adoptionRepository->findAll();
        $cats = $this->catRepository->findAll();*/

        /*$photo = new Picture();
        $photo->setFilename('https://images.unsplash.com/photo-1534351450181-ea9f78427fe8?ixid=MnwxMjA3fDB8MHxzZWFyY2h8OHx8ZG9nc3xlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60');
        $photo->setAdoption(null);
        $photo->setDog(null);
        $photo->setCat(null);
        $manager->persist($photo);


        $photo1 = new Picture();
        $photo1->setFilename('https://images.unsplash.com/photo-1534351450181-ea9f78427fe8?ixid=MnwxMjA3fDB8MHxzZWFyY2h8OHx8ZG9nc3xlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60');

        $photo1->setAdoption(null);
        $photo1->setDog(null);
        $photo1->setCat(null);
        $manager->persist($photo1);


        $photo2 = new Picture();
        $photo2->setFilename('https://www.woopets.fr/assets/races/000/102/bannerbig2021/cane-corso_2.jpg');
        $photo2->setAdoption(null);
        $photo2->setDog(null);
        $photo2->setCat(null);
        $manager->persist($photo2);

        $photo3 = new Picture();
        $photo3->setFilename('https://images.unsplash.com/photo-1534351450181-ea9f78427fe8?ixid=MnwxMjA3fDB8MHxzZWFyY2h8OHx8ZG9nc3xlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60');
        $photo3->setAdoption(null);
        $photo3->setDog(null);
        $photo3->setCat(null);
        $manager->persist($photo3);


        $photo4 = new Picture();
        $photo4->setFilename('https://images.unsplash.com/photo-1534351450181-ea9f78427fe8?ixid=MnwxMjA3fDB8MHxzZWFyY2h8OHx8ZG9nc3xlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60');
        $photo4->setAdoption(null);
        $photo4->setDog(null);
        $photo4->setCat(null);
        $manager->persist($photo4);


        $manager->flush();*/
    }
}
    public function getDependencies()
    {
        return [
            DogFixtures::class,
            PictureFixtures::class,
            CatFixtures::class
        ];
    }
}
