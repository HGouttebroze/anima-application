<?php

namespace App\Entity;

use App\Entity\Traits\HasIdTrait;
use App\Entity\Traits\HasNameTrait;
use App\Repository\AdoptionRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;

/*use Gedmo\Mapping\Annotation as Gedmo;*/

/**
 * @ORM\Entity(repositoryClass="App\Repository\AdoptionRepository", repositoryClass=AdoptionRepository::class)
 */
class Adoption
{

    use TimestampableEntity;
    use HasNameTrait;
    use HasIdTrait;


    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity=Contact::class, mappedBy="adoption")
     */
    private $contacts;

    /**
     * @ORM\OneToMany(targetEntity=Dog::class,
     *     mappedBy="adoption", cascade={"persist"})
     *
     */
    private $dogs;

    /**
     * @ORM\ManyToOne(targetEntity=User::class,
     *     inversedBy="adoptions")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=Association::class, inversedBy="Adoption")
     */
    private $association;

    /**
     * @ORM\OneToMany(targetEntity=Cat::class, mappedBy="adoption")
     */
    private $cats;

    /**
     * @ORM\OneToMany(targetEntity=Adoptant::class, mappedBy="adoption")
     */
    private $adoptants;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Picture", mappedBy="adoption", orphanRemoval=true, cascade={"persist"})
     */
    private $pictures;

    /**
     * @Assert\All({
     *   @Assert\Image(mimeTypes="image/jpeg")
     * })
     */
    private $pictureFiles;

    public function __construct()
    {
        $this->contacts = new ArrayCollection();
        $this->dogs = new ArrayCollection();
        $this->cats = new ArrayCollection();
        $this->adoptants = new ArrayCollection();
/*        $this->created_at = new \DateTime();
        $this->options = new ArrayCollection();*/
        $this->pictures = new ArrayCollection();
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }


    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }


    /**
     * @return \Doctrine\Common\Collections\Collection|Contact[]
     */
    public function getContacts(): Collection
    {
        return $this->contacts;
    }

    public function addContact(Contact $contact): self
    {
        if (!$this->contacts->contains($contact)) {
            $this->contacts->add($contact);
            $contact->setAdoption($this);
        }

        return $this;
    }

    public function removeContact(Contact $contact): self
    {
        if ($this->contacts->removeElement($contact)) {
            // set the owning side to null (unless already changed)
            if ($contact->getAdoption() === $this) {
                $contact->setAdoption(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Dog[]
     */
    public function getDogs(): Collection
    {
        return $this->dogs;
    }

    public function addDog(Dog $dog): self
    {
        if (!$this->dogs->contains($dog)) {
            $this->dogs[] = $dog;
            $dog->setAdoption($this);
        }

        return $this;
    }

    public function removeDog(Dog $dog): self
    {
        if ($this->dogs->removeElement($dog)) {
            // set the owning side to null (unless already changed)
            if ($dog->getAdoption() === $this) {
                $dog->setAdoption(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        $string = $this->getName();

        if (!empty($this->getDogs())) {
            $string .= ' (';
            foreach ($this->getDogs() as $key => $dog) {
                $string .= $dog->getName();
                if ($key < count($this->getDogs()) - 1) {
                    $string .= ', ';
                }
            }
            $string .= ')';
        }

        return $string;
    }

    public function isClosed(): bool
    {
        foreach ($this->getDogs() as $dog) {
            if (!$dog->getTaken()) {
                return false;
            }
        }

        return true;
    }

    public function getAssociation(): ?Association
    {
        return $this->association;
    }

    public function setAssociation(?Association $association): self
    {
        $this->association = $association;

        return $this;
    }

    /**
     * @return Collection|Cat[]
     */
    public function getCats(): Collection
    {
        return $this->cats;
    }

    public function addCat(Cat $cat): self
    {
        if (!$this->cats->contains($cat)) {
            $this->cats[] = $cat;
            $cat->setAdoption($this);
        }

        return $this;
    }

    public function removeCat(Cat $cat): self
    {
        if ($this->cats->removeElement($cat)) {
            // set the owning side to null (unless already changed)
            if ($cat->getAdoption() === $this) {
                $cat->setAdoption(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Adoptant[]
     */
    public function getAdoptants(): Collection
    {
        return $this->adoptants;
    }

    public function addAdoptant(Adoptant $adoptant): self
    {
        if (!$this->adoptants->contains($adoptant)) {
            $this->adoptants[] = $adoptant;
            $adoptant->setAdoption($this);
        }

        return $this;
    }

    public function removeAdoptant(Adoptant $adoptant): self
    {
        if ($this->adoptants->removeElement($adoptant)) {
            // set the owning side to null (unless already changed)
            if ($adoptant->getAdoption() === $this) {
                $adoptant->setAdoption(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Picture[]
     */
    public function getPictures(): Collection
    {
        return $this->pictures;
    }

    public function getPicture(): ?Picture
    {
        if ($this->pictures->isEmpty()) {
            return null;
        }
        return $this->pictures->first();
    }

    public function addPicture(Picture $picture): self
    {
        if (!$this->pictures->contains($picture)) {
            $this->pictures[] = $picture;
            $picture->setAdoption($this);
        }

        return $this;
    }

    public function removePicture(Picture $picture): self
    {
        if ($this->pictures->contains($picture)) {
            $this->pictures->removeElement($picture);
            // set the owning side to null (unless already changed)
            if ($picture->getAdoption() === $this) {
                $picture->setAdoption(null);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPictureFiles()
    {
        return $this->pictureFiles;
    }

    /**
     * @param mixed $pictureFiles
     * @return Adoption
     */
    public function setPictureFiles($pictureFiles): self
    {
/*        foreach($pictureFiles as $pictureFile) {
            $picture = new Picture();
            $picture->setImageFile($pictureFile);
            $this->addPicture($picture);
        }*/
        $this->pictureFiles = $pictureFiles;
        return $this;
    }

}
