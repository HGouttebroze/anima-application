<?php



namespace App\Form;

use App\Entity\Adoption;
use App\Entity\Contact;
use App\Entity\ContactUser;
use App\Form\Type\LocationType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('subject', TextType::class, [
                'trim' => true
            ])
            ->add('email', EmailType::class, [
                'trim' => true
            ])
            ->add('message', TextareaType::class, [
                'trim' => true
            ])
            ->add('adoption', EntityType::class, [
                'class' => Adoption::class,
            ])
            ->add('firstname')
            ->add('lastname')
            ->add('phone')
            ->add('location', LocationType::class, [
                'data_class' => Contact::class,
            ])
            ->add('birthDate')
            ->add('adoption',EntityType::class, [
                'class' => Adoption::class,
            ])


            // TODO: add user contact...
            /*->add('contactUser', EntityType::class, [
                'class' => ContactUser::class,
                'multiple' => true,
                'required' => false,
                'by_reference' => false,
            ])*/
        ;
    }

    /*
        BUG SUR 1 FORM (commande introduite sur la v.5.2):
        Utilisez la commande de debug:validator pour lister les contraintes de validation d'une classe donnée :
         php bin/console debug:validator 'App\Entity\SomeClass'
    */
    /* SOMES IMFORMATIONS ABOUT SYMFONY FORM TRAITEMENT & DEBUG */
    /* 'required' => true, */
    /* TO ADD A CONTRAINT DIRECTLY IN FORM: (look symfony's doc) */
    /* ->add('myField', TextType::class, [
         'constraints' => [new Length(['min' => 3])],
     ])
    */

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Contact::class,
        ]);
    }
}
